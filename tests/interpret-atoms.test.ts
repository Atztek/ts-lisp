import { expect } from 'chai';
import { run } from "../src";

describe('interpret', function () {
    describe('atoms', function () {
        it('should return string atom', function () {
            expect(run('"a"')).eql("a");
        });

        it('should return string with space atom', function () {
            expect(run('"a b"')).eql("a b");
        });

        it('should return string with opening paren', function () {
            expect(run('"(a"')).eql("(a");
        });

        it('should return string with closing paren', function () {
            expect(run('")a"')).eql(")a");
        });

        it('should return string with parens', function () {
            expect(run('"(a)"')).eql("(a)");
        });

        it('should return number atom', function () {
            expect(run('123')).eql(123);
        });
    });
});