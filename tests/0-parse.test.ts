import { expect } from 'chai';
import { Cell } from '../src/lib/base';
import { parse } from "../src/lib/parse";
import { Sym } from '../src/lib/symbol';


const unannotate = (exp: Cell): Array<any> => {
    const res = [];
    for (const item of exp) {
        if (item instanceof Sym) {
            res.push(item.toString())
            continue;
        }
        if ((item as Cell).car === null) {
            res.push([])
            continue;
        }
        if (item instanceof Cell) {
            res.push(unannotate(item));
            continue;
        }
        res.push(item);
    }
    return res;
}

//Проверка коректности работы парсера
describe('parser', function () {

    it('should lex a single atom', () => {
        const parsed = parse("a")
        expect((parsed.cdr as Cell).length).equal(1)
        expect(unannotate(parsed).pop()).equal("a")
    })


    it('should lex an atom in a list', function () {
        const parts = parse("()");
        expect(unannotate(parts.cdr as Cell).pop()).eql([]);
    });

    it('should lex multi atom', function () {
        const parts = parse("hi you")
        expect(unannotate(parts.cdr as Cell)).eql(["hi", "you"]);
    });

    it('should lex multi atom list', function () {
        const parts = parse("(hi you)")
        console.log(unannotate(parts))
        expect(unannotate(parts.cdr as Cell).pop()).eql(["hi", "you"]);
    });


    it('should lex multi atom list of lists', function () {
        const parts = parse("(hi yoo) (+ tes foo)")
        expect((parts.cdr as Cell).car).to.be.instanceof(Cell);
        expect((parts.cdr as Cell).cdr).to.be.instanceof(Cell);
    })

    /*
    ((lambda (x) x) 3)
    Cell 
        - car Cell
            - car lambda
            - cdr Cell
                - car Cell
                    - car Sym x
                    - cdr nil
                - cdr Cell
                    - car Sym - x 
                    - cdr nil
        - cdr Cell
            - car 3
            - cdr nil
    */
    it("should corect parse lambda invocation", () => {
        let parts = parse("((lambda (x) x) 3)");
        expect(((parts.cdr as Cell).car as Cell).car).to.not.be.null;
        parts = parse("( (lambda () (cdr (list 1 2 3)) ) )");
        // TODO: дописать проверку
    })

    describe('comment', () => {

        it('should correct parse inline comment', () => {
            expect(unannotate(parse("(hi you) ; comment")).pop()).eql(["hi", "you"]);
        });

        it('should correct parse multiline comment', () => {
            expect(unannotate(parse(`
                (hi you) 
                ;comment test
            `)).pop()).eql(["hi", "you"]);
        });
    });

    describe('quote', () => {
        it('check single quote parse', () => {

            let t = parse(`'123`)
            const qouteCell = (t.cdr as Cell).car as Cell
            expect(qouteCell.car).to.be.instanceof(Sym);
            expect((qouteCell.car as Sym).toString()).eq("quote");
            expect(qouteCell.cdr).to.be.instanceof(Cell);
            expect((qouteCell.cdr as Cell).car).eq(123)
        })

        it('check list quote parse', () => {
            let t = parse(`(car '(1 2 3) )`);
            const p = parse(`(car  (quote (1 2 3) ) )`)

            expect(unannotate(t)).deep.equal(unannotate(p))
        })

        it('check single quote in expresion', () => {
            let t = parse(`(register_indicator_object 
                                    'volatility_percent
                                    "get_pair_stat"
                                    "volatilityPercent"
                                    "period"
                                    (get_settings_value "volatility_period")
                            )`);
            let p = parse(`(register_indicator_object 
                                    (quote volatility_percent)
                                    "get_pair_stat"
                                    "volatilityPercent"
                                    "period"
                                    (get_settings_value "volatility_period")
                            )`);
            expect(unannotate(t)).deep.equal(unannotate(p))
        })

        it('check list quote in expresion', () => {
            let t = parse(`(test 
                                '(1 2 3)
                                sum
                            )`);
            let p = parse(`(test 
                                (quote (1 2 3))
                                sum
                            )`);
            expect(unannotate(t)).deep.equal(unannotate(p))
        })
    })
    /*
    it('should lex list containing list', function () {
        expect(unannotate(parse("((x))"))).eql([["x"]]);
    });
    
    it('should lex list containing list', function () {
        expect(unannotate(parse("(x (x))")).pop()).eql(["x", ["x"]]);
    });

    it('should lex list containing list', function () {
        expect(unannotate(parse("(x y)")).pop()).eql(["x", "y"]);
    });

    it('should lex list containing list', function () {
        expect(unannotate(parse("(x (y) z)")).pop()).eql(["x", ["y"], "z"]);
    });

    it('should lex list containing list', function () {
        expect(unannotate(parse("(x (y) (a b c))")).pop()).eql(["x", ["y"], ["a", "b", "c"]]);
    });

    describe('atoms', function () {
        it('should parse out numbers', function () {
            expect(unannotate(parse("(1 (a 2))")).pop()).eql([1, ["a", 2]]);
        });

        it('should parse out bools', function () {
            expect(unannotate(parse("(#t #f)")).pop()).eql([true, false]);
        });

    });

    describe('utils', () => {
        /*
        it('should return Symbols with args by set', () => {
            expect(parse("(sum 10 20)").atomsBySumbolsSet(new Set(["sum"]))).eql(new Set([
                { sym: 'sum', args: Tree.from([10, 20]) }
            ]));
        })
        
    });

    describe('symbols', () => {
        it('should return unique set of using simbols', () => {
            expect(usedSymbolsSet("(a 10 20 (+ 20 30) (list 20 30 40))")).eql(new Set(["begin", "a", "+", "list"]))
        })
    })
    */
});
