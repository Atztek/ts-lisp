import { expect } from 'chai';
import { run } from "../src";

describe('interpret', function () {
    describe('if', function () {
        it('should choose the right branch', function () {
            expect(run("(if 1 42 4711)")).eql(42);
            expect(run("(if 0 42 4711)")).eql(4711);
            expect(run("(if 1 42)")).eql(42);
            expect(run("(if 0 42)")).eql(undefined);

            expect(run("(if (= #t #t) 42)")).eql(42);

            // TODO: добавить проверку функций в качестве условия


        });
    });

    describe('when', () => {

        it('should choose the right branch', () => {
            expect(run("(when 1 42)")).eql(42);
            expect(run("(when 0 42)")).eql(undefined);
            expect(run("(when (= #t #t)  42)")).eql(42);
        });
    });
});
