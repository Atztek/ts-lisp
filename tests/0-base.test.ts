import { expect } from 'chai';
import { Cell } from '../src/lib/base';
import { Sym } from '../src/lib/symbol';


//Проверка коректности работы парсера
describe('base', function () {

    it('test cons Cell', () => {
        expect(new Cell(1, new Cell(2, null)).length).equal(2)
    })

    it('test cell string repr', () => {
        expect(
            new Cell(10, 20)
                .toString()
        ).equal("( 10 . 20 )")

        expect(
            new Cell(
                10,
                new Cell(
                    20,
                    null))
                .toString()
        ).equal("( 10 20 )")

        expect(
            new Cell(
                Sym.interned("hi"),
                new Cell(
                    Sym.interned("you"),
                    new Cell(
                        Sym.interned('foo'),
                        null)))
                .toString()
        ).eq("( hi you foo )")

        expect(
            new Cell(
                Sym.interned("hi"),
                new Cell(
                    new Cell(
                        Sym.interned("you"),
                        new Cell(
                            10,
                            null)),
                    null
                )).toString()).eq("( hi ( you 10 ) )")

        expect(
            new Cell(
                new Cell(10, new Cell(20, null)),
                new Cell(
                    new Cell(30, new Cell(40, null)),
                    null
                )
            ).toString()
        ).eq("( ( 10 20 ) ( 30 40 ) )")
    })

    it("test Sym class", () => {
        let t = Sym.interned('test')
        expect(t.toString()).equal('test')
    })

});
