import { expect } from 'chai';
import { run } from "../src";

describe('interpret', function () {
    describe('mainlib', () => {
        describe('begin', () => {
            it('check begin eval', () => {
                expect(run("(begin (define x 10) (set! x 3) x)")).eql(3);
            });

            it('should corect wrap begin', () => {
                expect(run("(define x 10) (set! x 3) x")).eql(3);
            });

            it('should correct wrap lambda in begin block', function () {
                expect(run("((lambda (x) (begin (+ x 10) (+ x 20)) ) 10)")).eql(30);
            });
        });

        describe('car / cdr', () => {
            it('should return first element of list', function () {
                expect(run("(car (list 1 2 3))")).eql(1);
            });

            it('should generate car: to many arguments error', function () {
                expect(() => { run("(car (list 1 2 3) 20)") }).to.throw("car: too many arguments");
            });



            it('should return rest of list', function () {
                expect(run("(cdr (list 1 2 3))")).eql([2, 3]);
            });

            it('should generate cdr: to many arguments error', function () {
                expect(() => { run("(cdr (list 1 2 3) 20)") }).to.throw("cdr: too many arguments");
            });
        });
    })
    /*
  

        

        describe('print', () => {
            it('should corect call display function', function () {
                let oldLog = console.log;
                console.log = (message) => {
                    throw message;
                };
                expect(() => {
                    run('(display "em" "am")')
                }).to.throw('emam');
                console.log = oldLog;
            });

            it('should throw display error', () => {
                expect(() => { run("(display )") }).to.throw("error: display not enough arguments");
            });
        })
    });
    */

});