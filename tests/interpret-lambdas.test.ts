
import { expect } from 'chai';
import { run } from "../src";

describe('interpret', function () {
    describe('lambdas', function () {
        it('should return correct result when invoke lambda w no params', function () {
            expect(run("( (lambda () (cdr (list 1 2 3)) ) )")).eql([2, 3]);
        });

        it('should return correct result for lambda that takes and returns arg', function () {
            expect(run("((lambda (x) 10 x) 3)")).eql(3);
        });

        it('should return correct result for lambda that returns list of vars', function () {
            expect(run("((lambda (x y) (list x y)) 1 2)")).eql([1, 2]);
        });

        it('should get correct result for lambda that returns list of lits + vars', function () {
            expect(run("((lambda (x y) (list 0 x y)) 1 2)")).eql([0, 1, 2]);
        });


        it('should get correct result for lambda min', function () {
            expect(run("(define min (lambda (a b) (if (> a b) b a) ) )  (min 10 5)")).eql(5);
            expect(run("(define min (lambda (a b) (if (> a b) b a) ) )  (min 5 10)")).eql(5);

            expect(run("(define second ( lambda(x) ( car (cdr x) ) )"))

        });

        it('should return correct result when invoke lambda w params', function () {
            expect(run("( ( lambda (x) (car x) ) (list 1 2 3) )")).eql(1);
        });


        it('should correct wrap lambda in begin block', function () {
            expect(run("((lambda (x) (+ x 10) (+ x 20) ) 10)")).eql(30);
        });
    });
});