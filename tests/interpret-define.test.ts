import { expect } from 'chai';
import { run } from "../src";


describe('interpret', function () {
    describe('define', function () {
        it('should corect add define var', function () {
            expect(run("(define x 10)"));
        });
        // define работает неправильно
        it('should corect add define var to scope', function () {
            expect(run("(define x 10) (+ x 5) (+ x 3)")).eql(13);
        });

        it('should corect add define func to scope', function () {
            expect(run("(define add ( lambda (z) (+ z 1) ) ) (add 3)")).eql(4);
        });

    });

    describe('set!', () => {
        it('should corect set var in scope', () => {
            expect(run("(define x 10) (set! x 3) x")).eql(3);
        });

        it('should corect set var in lambda', () => {
            expect(run("(define z 10) (define some (lambda () (set! z 20))) (some) z")).eql(20);
        });

        it('should throw error if var not define in scope', () => {
            expect(() => { run("(set! x 3) x") }).to.throw("identifier \"x\" not found");
        });
    });

});