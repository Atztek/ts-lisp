import { expect } from 'chai';
import { run, Sym } from "../src";
import { Cell } from '../src/lib/base';
import { evaluate, evalList } from '../src/lib/interpret/index';

describe('check outer lib', () => {

    Sym.addEvalRaw(Sym.interned("raweval"));

    let lib = {
        to_lisp: (...args: Array<any>) => {
            return Array.from(args);
        },
        print: (input: any, context: any) => {
            return evalList(input as Cell, context).join("")
        },

        make: (...args: Array<any>) => {
            console.log(args);
            let [fun, ...rest] = args;
            return fun;
        },

        raweval: (cell: Cell, context: any) => {
            return cell;
        }
    }

    it('should correct use a outer lib', () => {
        expect(run('(to_lisp 1 2 3 4)', lib)).eql([1, 2, 3, 4]);
    });


    it('should correct use a outer lib with scopes', () => {
        let func = run(`
        (define z 10)
        (make (lambda (x) (print (+ x z) ) ))
        `, lib);
        // FIXME: bug
        //expect(func.apply(false, [10])).eq("20");
    });

    it('should correctly eval raw symbols', () => {
        let cell = run("(raweval 1 2 3)", lib);
        expect(cell).to.be.instanceOf(Cell);
    })


    it('should correct redefine main lib functions', () => {
        expect(run('(print "me")', lib)).eql("me");
        expect(run('((lambda () (print "me")))', lib)).eql("me");
    });

    it('outer lib do not override main env', () => {
        expect(run('(to_lisp 1 2 3 4) (+ 1 2)', lib)).eql(3);
    });

})