import { expect } from 'chai';
import { run } from "../src";


describe('interpret', function () {
    describe('conditions', function () {


        it('should corect equal cond', function () {
            expect(run('(= 10 10)')).eql(true);
            expect(run('(= 10 10 10)')).eql(true);
            expect(run('(= 10 10 20)')).eql(false);
            expect(run('(= 10 "10")')).eql(false);
            expect(()=>{ run('(= 10)') }).to.throw("Error: =: too few arguments (at least: 2 got: 1) [=]")
        });

        it('should corect greater cond', function () {
            expect(run('(> 11 10)')).eql(true);
            expect(run('(> 11 10 9)')).eql(true);
            expect(run('(> 15 12 13)')).eql(false);
            expect(run('(> 10 10)')).eql(false);
            expect(()=>{ run('(> 10)') }).to.throw("Error: >: too few arguments (at least: 2 got: 1) [>]")
        });

        it('should corect lower cond', function () {
            expect(run('(< 10 11)')).eql(true);
            expect(run('(< 10 15 20)')).eql(true);
            expect(run('(< 10 15 12)')).eql(false);
            expect(run('(< 10 10)')).eql(false);
            expect(()=>{ run('(< 10)') }).to.throw("Error: <: too few arguments (at least: 2 got: 1) [<]")
        });

        it('should corect greater or eqal cond', function () {
            expect(run('(>= 11 10)')).eql(true);
            expect(run('(>= 10 10)')).eql(true);
            expect(run('(>= 9 10)')).eql(false);
            expect(()=>{ run('(>= 10)') }).to.throw("Error: >=: too few arguments (at least: 2 got: 1) [>=]")
        });

        it('should corect lower or eqal cond', function () {
            expect(run('(<= 10 11)')).eql(true);
            expect(run('(<= 10 10)')).eql(true);
            expect(run('(<= 10 9)')).eql(false);
            expect(()=>{ run('(<= 10)') }).to.throw("Error: <=: too few arguments (at least: 2 got: 1) [<=]")
        });

    });
});