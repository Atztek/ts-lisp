import { expect } from 'chai';
import { run } from "../src";

describe('interpret', function () {
    describe('lists', function () {
        it('should return empty list', function () {
            expect(run('()')).eql([]);
        });

        it('should return list of strings', function () {
            expect(run('(list "hi" "mary" "rose")')).eql(['hi', "mary", "rose"]);
        });

        it('should return list of numbers', function () {
            expect(run('(list 1 2 3)')).eql([1, 2, 3]);
        });

        it('should return list of numbers in strings as strings', function () {
            expect(run('(list "1" "2" "3")')).eql(["1", "2", "3"]);
        });
    });
});