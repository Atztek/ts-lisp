import { expect } from 'chai';
import { run } from "../src";

describe('interpret', function () {
    describe('invocation', function () {

        it('invocation on list should throw error', function () {
            expect(() => { run("((list 1 2 3))") }).to.throw('Error: "1,2,3" is not a function');
        });

        it('invocation error', function () {
            expect(() => { run("( (cdr (list 1 2 3)) )") }).to.throw('Error: "2,3" is not a function');
        });

    });
});