import { expect } from 'chai';
import { run } from "../src";

describe('interpret', function () {
    describe('quote', function () {
        it('check `quote` sym', function () {
            expect(run("(quote 1)")).eql(1);
            expect(run("(quote 1 2)")).eql(1);
            const t = run("(quote (1 2))")
            expect(Array.from(t)).eql([1, 2]);

        });


        it('check quote single sym', () => {
            expect(run(`(car '(1 2 3))`)).eql(1);
        });
    });
});
