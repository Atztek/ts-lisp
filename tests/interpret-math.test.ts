import { expect } from 'chai';
import { run } from "../src";

describe('interpret', function () {
    describe('math', function () {
        describe('sum', function () {
            it('should return corect sum for 1 + 2', function () {
                expect(run('(+ 1 2)')).eql(3);
            });

            it('should return corect sum for 1 + 2 + 3 + 4', function () {
                expect(run('(+ 1 2 3 4)')).eql(10);
            });
        });

        describe('multiple', function () {

            it('should return corect multiple for 2 * 2 ', function () {
                expect(run('(* 2 2)')).eql(4);
            });

            it('should return corect multiple for 1 * 2 * 3', function () {
                expect(run('(* 1 2 3)')).eql(6);
            });
        });

        describe('subtract', function () {
            it('should return corect subtraction for 2 - 1 ', function () {
                expect(run('(- 2 1 )')).eql(1);
            });

            it('should return corect subtraction for 4 - 2 -1', function () {
                expect(run('(- 4 2 1)')).eql(1);
            });
        });

        describe('division', function () {
            it('should return corect division for 4 / 2', function () {
                expect(run('(/ 4 2)')).eql(2);
            });

            it('should return corect division for 4 / 2 / 2', function () {
                expect(run('(/ 4 2 2)')).eql(1);
            });
        });

        describe('mod', function () {
            it('should return corect division for 5 mod 2', function () {
                expect(run('(mod 5 2)')).eql(1);
            });
        });
    });
});