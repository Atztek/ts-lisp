import { expect } from 'chai';
import { run } from "../src";


describe('interpret', () => {
    describe('logick', () => {

        it('should corect and logical operator', function () {
            expect(run('(and #t #t)')).eql(true);
            expect(run('(and #t #t #t)')).eql(true);
            expect(run('(and #t #t #f)')).eql(false);
            expect(run('(define true_val #t) (and #t #t true_val)')).eql(true);
        });

        it('should corect or logical operator', function () {
            expect(run('(or #t #t)')).eql(true);
            expect(run('(or #t #f)')).eql(true);
            expect(run('(or #t #f #t)')).eql(true);
            expect(run('(or #f #f)')).eql(false);
            expect(run('(or #f #f #f)')).eql(false);
        });

        it('should corect not logical operator', () => {
            expect(run('(not #t)')).eql(false);
            expect(run('(not #f)')).eql(true);
            expect(() => { run('(not #f #f)') }).to.throw("not: too many arguments");
        })

    });
});