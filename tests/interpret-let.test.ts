
import { expect } from 'chai';
import { run } from "../src";


describe('interpret', function () {
    describe('let', function () {
        it('should eval inner expression w names bound', function () {
            expect(run("(let ((x (+ 1 2)) (y 2)) (list x y))")).eql([3, 2]);
        });
        it('should not expose parallel bindings to each other', function () {
            // Expecting undefined for y to be consistent with normal
            // identifier resolution in littleLisp.
            expect(() => { run("(let ((x 1) (y x)) (list x y))") }).to.throw("identifier \"x\" not found");
        });

        it('should accept empty binding list', function () {
            expect(run("(let () 42)")).eql(42);
        });
    });
});