import { evaluate, evalList } from './lib/interpret';
import { parse } from './lib/parse';
import { Context } from './lib/context';
import { LispLibrary, special } from './lib/interpret/library';
import { Sym } from './lib/symbol';

const run = (str: string, lib: undefined | LispLibrary = undefined) => {
    let context = undefined;
    if (lib) {
        let newLib = Object.assign(Object.assign({}, special), lib)
        Object.assign(lib, newLib);
        context = new Context(lib);
    }
    let parsed = parse(str);
    return evaluate(parsed, context);
}

export {
    parse,
    run,
    evalList,
    evaluate,
    Context,
    Sym
};