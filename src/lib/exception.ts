export class ImproperListException extends Error {
    constructor(public readonly tail: unknown) {
        super();
    }
}