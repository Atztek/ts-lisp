import { Context } from '../context';
import { Atom } from '../parse';

import { Sym } from '../symbol';
import { Cell } from '../base';
import { special } from './library';



export const evaluate = (expression: Atom, context: Context | undefined = undefined): any => {
    if (context === undefined) {
        return evaluate(expression, new Context(Object.assign({}, special)));
    }

    if (expression instanceof Cell) {
        if (expression.nill) return [];


        if (Sym.mayEvalRaw(expression.car as Sym)) {
            const dargs = (expression.cdr as Cell)
            return context.evalScope(expression.car as Sym)(dargs, context);
        }

        let [f, ...args] = evalList(expression, context);
        if (f instanceof Function) {
            return f(...args)
        } else {
            throw `Error: "${f}" is not a function`;
        }
    }

    if (expression instanceof Sym) return context.evalScope(expression);

    return expression
}

export const evalList = (input: Cell, context: Context) => {
    const retArray = []
    for (let i of input) {
        retArray.push(evaluate(i as Atom, context));
    }
    return retArray;
}
