import { Context } from '../../context';
import { Cell } from '../../base';
import { evaluate } from '../index';

const library = {
    car: (...args: Array<any>) => {
        if (args.length != 1) {
            throw "car: too many arguments"
        }
        const t = args[0];
        if (t instanceof Cell) {
            return t.car;
        }
        return t.shift();
    },

    cdr: (...args: Array<any>) => {
        if (args.length != 1) {
            throw "cdr: too many arguments"
        }
        const t = args[0];
        if (t instanceof Cell) {
            return t.cdr;
        }
        return t.slice(1);
    },

    /*
    display: function (args: any, context: Context) {
        if (args.length == 0) throw "error: display not enough arguments";
        console.log(Array.from(args.map((atom: Atom) => {
            return interpret(atom, context);
        })).join(""));
    }
    */
};

export { library }