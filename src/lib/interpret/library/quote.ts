import { Cell } from '../../base';
import { Context } from '../../context';

const quote = {
    quote: (input: Cell) => {
        return input.car;
    }
}

export default quote;