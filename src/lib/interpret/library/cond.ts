import { Cell } from "../../base";
import { slidingWindow } from "../../utils";
import { evalList } from "..";

const reducer = (list: Array<any>, comparator: Function) => {
    return list.reduce((prevVal: any, currentValue: any) => {
        const [first, second] = currentValue;
        return comparator(first, second) && prevVal
    }, true);
}

const  checkArgsGTE = (funct: string, need: number, cur: number) => {
    if(need > cur){
        throw `Error: ${funct}: too few arguments (at least: ${need} got: ${cur}) [${funct}]`
    }
}


let conds = {
    '=': (...args: Array<any>): boolean => {
        checkArgsGTE("=", 2, args.length);
        const list = slidingWindow(args)
        return reducer(list, (x: any, y: any) => x === y);
    },
    '<': (...args: Array<any>): boolean => {
        checkArgsGTE("<", 2, args.length);
        const list = slidingWindow(args)
        return reducer(list, (x: any, y: any) => x < y);
    },
    '>': (...args: Array<any>): boolean => {
        checkArgsGTE(">", 2, args.length);
        const list = slidingWindow(args)
        return reducer(list, (x: any, y: any) => x > y);
    },
    '<=': (...args: Array<any>): boolean => {
        checkArgsGTE("<=", 2, args.length);
        const list = slidingWindow(args)
        return reducer(list, (x: any, y: any) => x <= y);
    },
    '>=': (...args: Array<any>): boolean => {
        checkArgsGTE(">=", 2, args.length);
        const list = slidingWindow(args)
        return reducer(list, (x: any, y: any) => x >= y);
    }
}

export default conds;