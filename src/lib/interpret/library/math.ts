import { Context } from '../../context';
import { Cell } from "../../base";
import { evalList } from '..';

let math = {
    '+': (...args: Array<number>) => {
        return args.reduce((accumulator: number, current: number) => {
            return accumulator + current;
        });
    },

    '-': (...args: Array<number>) => {
        return args.reduce((accumulator: number, current: number) => {
            return accumulator - current;
        });
    },

    '/': (...args: Array<number>) => {
        return args.reduce((accumulator: number, current: number) => {
            return accumulator / current;
        });
    },

    '*': (...args: Array<number>) => {
        return args.reduce((accumulator: number, current: number) => {
            return accumulator * current;
        });
    },

    mod: (...args: Array<number>) => {
        // TODO: проверка аргументов на длину
        let [first, second] = args
        return first % second;
    }
}

export default math;