import { library } from "./library";
import maths from "./math";
import lists from "./list";
import logicks from "./logick";
import conds from "./cond";
import quote from "./quote";
import { Atom } from "../../parse";
import { Cell } from '../../base';
import { BeginSym, Sym } from "../../symbol";
import { evalList, evaluate } from "..";
import { Context } from "../../context";

export type LispLibrary = { [key: string]: any };

var special: LispLibrary = {
    // import maths
    ...maths,

    // import lists
    ...lists,

    // import conditions
    ...conds,

    // import logicks
    ...logicks,

    ...library,

    // import quote
    ...quote,

    begin: (...args: Array<any>): any => {
        return args.pop();
    },

    let: function (input: any, context: any): any {
        const letContext = new Context({}, context);
        for (let epx of input.car) {
            if (epx) { // TODO: в конце не должно быть null исправить
                letContext.addToScope(epx.car as Sym, evalList(epx.cdr, context).pop())
            }
        }
        return evalList(input.cdr, letContext).pop();
    },

    'set!': (input: Cell, context: Context): any => {
        let [value] = evalList(input.cdr as Cell, context);
        context.setScopeVar(input.car as Sym, value);
    },

    define: (input: any, context: Context): any => {
        let [value] = evalList(input.cdr, context);
        context.addToScope(input.car, value);
    },

    lambda: (input: Cell, context: any): any => {
        const args: string[] = [];
        for (const item of input.car as Cell) {
            args.push(String(item))
        }

        return function () {
            let lambdaArguments: any[] = [];

            if (arguments[0] instanceof Cell) {
                lambdaArguments = evalList(arguments[0], context);
            } else {
                // заглушка для вызова функции извне
                lambdaArguments = Array.from(arguments);
            }


            let lambdaScope = args.reduce(function (acc: any, x: any, i: number) {
                acc[x] = evaluate(lambdaArguments[i], context);
                return acc;
            }, {});

            let body = input.cdr as Cell;
            if ((body.car as Cell).car !== BeginSym) {
                body = new Cell(BeginSym, body)
            } else {
                body = body.car as Cell;
            }
            /*
            if (input.length >= 3) {
                body = new Tree();
                body.push(new Symbol("begin"));
                body = body.concat(input.slice(1));
            }
            */
            const t = evaluate(body, new Context(lambdaScope, context));
            return t;
        };
    },

    if: function (input: Cell, context: Context): any {


        // TODO:  проверка input.cdr.length >= 1
        const [cond, first, second] = Array.from(input as Iterable<any>);
        return evaluate(cond, context) ?
            evaluate(first, context) :
            evaluate(second, context);
    },
    when: function (input: Cell, context: Context): any {
        const [cond, first] = Array.from(input as Iterable<any>);
        return evaluate(cond, context) ?
            evaluate(first, context) : undefined;
    }

};
export { special }