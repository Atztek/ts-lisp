import { Context } from '../../context';
import { evaluate } from "..";
import { Cell } from "../../base";

// TODO: убрать атомы
let logick = {
    // TODO: add lazy
    'and': (...args: Array<any>) => {

        return args.reduce((accumulator: boolean | number, current: boolean) => {
            return accumulator && current
        });
    },
    'or': (...args: Array<any>) => {
        return args.reduce((accumulator: boolean | number, current: boolean) => {
            return accumulator || current
        });
    },
    'not': (...args: Array<any>) => {
        if (args.length > 1) {
            throw Error("not: too many arguments")
        }
        return !args[0];
    }
}

export default logick;