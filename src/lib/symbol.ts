class Sym {
    // Construct a symbol that is not interned yet.
    private constructor(private readonly name: string) { }

    toString(): string { return this.name; }

    // The table of interned symbols
    private static symbols: { [name: string]: Sym } = {};

    // The table of interned symbols
    private static evalRaw: Set<Sym> = new Set();

    // Construct an interned symbol.
    static interned(name: string, raw = false): Sym {
        let result = Sym.symbols[name];
        if (result === undefined) {
            result = new Sym(name);
            Sym.symbols[name] = result;
            if (raw) {
                Sym.addEvalRaw(result);
            }
        }
        return result;
    }

    static get usedSymbols() {
        return Sym.symbols
    }


    static addEvalRaw(sym: Sym) {
        Sym.evalRaw.add(sym);
    }

    static mayEvalRaw(sym: Sym) {
        return Sym.evalRaw.has(sym);
    }
}

const QuoteSym = Sym.interned('quote', true);
const IfSym = Sym.interned('if', true);
const WhenSym = Sym.interned('when', true);
const BeginSym = Sym.interned('begin');
const LambdaSym = Sym.interned('lambda', true);
const DefineSym = Sym.interned('define', true);
const SetQSym = Sym.interned('set!', true);
const LetSym = Sym.interned('let', true);
const ApplySym = Sym.interned('apply');
const CallCCSym = Sym.interned('call/cc');

export {
    Sym,
    QuoteSym,
    IfSym,
    LetSym,
    BeginSym,
    LambdaSym,
    DefineSym,
    SetQSym,
    ApplySym,
    CallCCSym,
}