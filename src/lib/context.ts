// Класс контекста
import { LispLibrary } from "./interpret/library";
import { Sym } from './symbol';
export class Context {
    scope: LispLibrary = {};
    parent: Context | undefined = undefined;

    constructor(scope: LispLibrary, parent: any = undefined) {
        this.scope = scope;
        this.parent = parent;
    }

    addToScope(sym: Sym, func: any) {
        this.scope[sym.toString()] = func;
    }

    setScopeVar(sym: Sym, func: any) {
        if (this.scope.hasOwnProperty(sym.toString())) {
            this.scope[sym.toString()] = func;
        } else if (this.parent !== undefined) {
            this.parent.setScopeVar(sym, func);
        } else {
            throw `identifier "${sym.toString()}" not found`;
        }
    }

    evalScope(sym: Sym) {
        let atom = this.get(sym);
        return atom;
    }

    get(sym: Sym): any {
        if (sym.toString() in this.scope) {
            return this.scope[sym.toString()];
        } else if (this.parent !== undefined) {
            return this.parent.get(sym);
        }
        throw `identifier "${sym.toString()}" not found`;
    };
}