import { ImproperListException } from "./exception";

// Lisp cons cell  https://ru.wikipedia.org/wiki/Cons
export class Cell {
    constructor(public car: unknown,
        public cdr: unknown, public functional = false) { }

    // Yield car, cadr, caddr and so on.
    [Symbol.iterator]() {
        let j: unknown = this;
        return {
            next: () => {
                if (j === null) {
                    return {
                        done: true
                    };
                } else if (j instanceof Cell) {
                    let val = j.car;
                    j = j.cdr;
                    return {
                        done: false,
                        value: val
                    };
                } else {
                    throw new ImproperListException(j);
                }
            }
        }
    }
    // This is slightly faster than *[Symbol.iterator]() {... yield j.car ...}.

    // Length as a list
    get length(): number {
        let i = 0;
        for (const e of this) i++;
        return i;
    }

    get nill(): boolean {
        return this.car === null && this.cdr === null;
    }

    toString(isCont = false): string {
        if (this.cdr === null) { // end of list
            return `${this.car}`
        }

        if (this.isList) {
            const last = `${this.car} ${(this.cdr as Cell).toString(true)}`;
            if (isCont) {
                return last;
            }
            return `( ${last} )`;
        } else {
            return `( ${this.car} . ${this.cdr} )`;
        }
    }

    get isList() {
        return this.cdr instanceof Cell
    }
}




