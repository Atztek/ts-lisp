export function slidingWindow(inputArray: Array<any>, size: number = 2) {
    return Array.from(
        { length: inputArray.length - (size - 1) }, //get the appropriate length
        (_, index) => inputArray.slice(index, index + size) //create the windows
    )
}