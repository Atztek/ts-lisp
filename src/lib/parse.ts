import { Cell } from './base';
import { Sym, QuoteSym } from './symbol';


export type List = Cell | null;

export type Atom = Sym | number | boolean | Array<any> | List;



export const tokenGenearator = function* (input: string) {
    const readLine = function* () {
        let matches = input.match(/[^\r\n]+/g) || [];
        for (let line of matches) {
            yield line;
        }
        yield "";
    }
    let lines = readLine();
    let line = "";
    while (true) {
        if (line == "") line = <string>lines.next().value;
        if (line == "") return "";

        let [, token, restLine] = line.match(/\s*(,@|[('`,)]|"(?:[\\].|[^\\"])*"|;.*|[^\s('"`,;)]*)(.*)/) || [];
        line = restLine;
        if (token[0] != ";" && token != "") {
            yield token;
        }
    }
}


const categorize = function (input: any): any {
    if (input == '#t') return true;
    if (input == '#f') return false;
    if (!isNaN(parseFloat(input))) return parseFloat(input)
    if (input[0] === '"' && input.slice(-1) === '"') {
        return input.slice(1, -1);
    } else {
        return Sym.interned(input);
    }
};

export const parse = (input: string): Cell => {
    let current = new Cell(Sym.interned('begin'), null, true);

    let top = current;
    const stack: Array<any> = [];

    let functional = false;

    let quoteNext = false;
    let quoteLevel = 0;

    for (const token of tokenGenearator(input)) {
        let nexListItem = new Cell(null, null, functional);
        if (functional) {
            current.car = nexListItem;
        } else {
            current.cdr = nexListItem;
        }
        let prev = current
        current = nexListItem;

        functional = false;

        switch (token) {
            case '(':
                functional = true;
                stack.push(nexListItem)
                if (quoteNext) {
                    quoteLevel++;
                }
                break
            case `'`:
                current.car = new Cell(QuoteSym, null, true);
                stack.push(current)
                current = current.car as Cell
                quoteNext = true;
                break
            case ')':
                prev.cdr = null;
                current = stack.pop();

                if (quoteNext) {
                    quoteLevel--;
                }


                if (quoteNext && quoteLevel == 0) {
                    current = stack.pop();
                    quoteNext = false
                }

                continue
            default:
                current.car = categorize(token)

                if (quoteNext && quoteLevel == 0) {
                    current = stack.pop();
                    quoteNext = false;
                }
        }
    }
    return top as Cell;
}

